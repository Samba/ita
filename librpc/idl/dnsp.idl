#include "idl_types.h"

/*
   IDL structures for DNSP structures

   See [MS-DNSP].pdf in MCPP for details
*/

import "misc.idl";

/*
  note that this is not a real RPC interface. We are just using PIDL
  to save us a lot of tedious hand parsing of the dnsRecord
  attribute. The uuid is randomly generated.
 */
[
  uuid("bdd66e9e-d45f-4202-85c0-6132edc4f30a"),
  version(0.0),
  pointer_default(unique),
  helper("../librpc/ndr/ndr_dnsp.h"),
  helpstring("DNSP interfaces")
]

interface dnsp
{
	typedef enum {
		DNS_TYPE_ZERO  = 0x0,
		DNS_TYPE_A     = 0x1,
		DNS_TYPE_NS    = 0x2,
		DNS_TYPE_MD    = 0x3,
		DNS_TYPE_MF    = 0x4,
		DNS_TYPE_CNAME = 0x5,
		DNS_TYPE_SOA   = 0x6,
		DNS_TYPE_MB    = 0x7,
		DNS_TYPE_MG    = 0x8,
		DNS_TYPE_MR    = 0x9,
		DNS_TYPE_NULL  = 0xA,
		DNS_TYPE_WKS   = 0xB,
		DNS_TYPE_PTR   = 0xC,
		DNS_TYPE_HINFO = 0xD,
		DNS_TYPE_MINFO = 0xE,
		DNS_TYPE_MX    = 0xF,
		DNS_TYPE_RP    = 0x11,
		DNS_TYPE_AFSDB = 0x12,
		DNS_TYPE_X25   = 0x13,
		DNS_TYPE_ISDN  = 0x14,
		DNS_TYPE_RT    = 0x15,
		DNS_TYPE_SIG   = 0x18,
		DNS_TYPE_KEY   = 0x19,
		DNS_TYPE_AAAA  = 0x1C,
		DNS_TYPE_LOC   = 0x1D,
		DNS_TYPE_NXT   = 0x1E,
		DNS_TYPE_SRV   = 0x21,
		DNS_TYPE_ATMA  = 0x22,
		DNS_TYPE_NAPTR = 0x23,
		DNS_TYPE_DNAME = 0x27,
		DNS_TYPE_DS    = 0x2B,
		DNS_TYPE_RRSIG = 0x2E,
		DNS_TYPE_NSEC  = 0x2F,
		DNS_TYPE_DNSKEY= 0x30,
		DNS_TYPE_DHCID = 0x31,
		DNS_TYPE_ALL   = 0xFF,
		DNS_TYPE_WINS  = 0xFF01,
		DNS_TYPE_WINSR = 0xFF02
	} dns_record_type;

	typedef [nodiscriminant] union {
		[case(DNS_TYPE_A)] [flag(NDR_BIG_ENDIAN)] ipv4address ip;
		[case(DNS_TYPE_NS)]                       dnsp_name ns;
		[default] [flag(NDR_REMAINING)] DATA_BLOB data;
	} dnsRecordData;

	/* this is the format for the dnsRecord attribute in the DNS
	   partitions in AD */
	typedef [public] struct {
		uint16          wDataLength;
		dns_record_type wType;
		uint32          dwFlags;
		uint32          dwSerial;
		uint32          dwTtlSeconds;
		uint32          dwTimeStamp;
		uint32          dwReserved;
		[switch_is(wType)] dnsRecordData data;
	} dnsp_DnssrvRpcRecord;


	/*
	  this is a convenience hook for ndrdump
	 */
	void decode_DnssrvRpcRecord(
		[in] dnsp_DnssrvRpcRecord blob
		);
}
