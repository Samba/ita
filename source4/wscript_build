#!/usr/bin/env python

# top level waf build script for samba4

import os
srcdir = ".."

# create separate build groups for building the asn1 and et compiler, then
# building the C from ASN1 and IDL, and finally the main build process
bld.SETUP_BUILD_GROUPS()

bld.SAMBA_MKVERSION('version.h')

# bld.ENABLE_MAGIC_ORDERING()

# we don't always use consistent names for our subsystems.
# this sets up some aliases
bld.TARGET_ALIAS('attr', 'XATTR')
bld.TARGET_ALIAS('sasl2', 'SASL')
bld.TARGET_ALIAS('RPC_NDR_SRVSVC', 'NDR_SRVSVC')
bld.TARGET_ALIAS('RPC_NDR_MGMT', 'dcerpc_mgmt')

# missing subsystems - need looking at
bld.SAMBA_SUBSYSTEM('NETAPI', '')       # for external libnet
bld.SAMBA_SUBSYSTEM('OPENPTY', '')      # external openpty library
bld.SAMBA_SUBSYSTEM('SMBCLIENT', '')    # for external libsmbclient
bld.SAMBA_SUBSYSTEM('BLKID', '')        # blkid library from e2fsprogs
bld.SAMBA_SUBSYSTEM('SETPROCTITLE', '') # external setproctitle library

# subsystems that are confirmed to be missing. These should
# be removed from the build completely when we only have
# one build system
bld.SAMBA_SUBSYSTEM('DCOM_PROXY_DCOM', '')
bld.SAMBA_SUBSYSTEM('smbcalls', '')
bld.SAMBA_SUBSYSTEM('HDB_LDB', '')
bld.SAMBA_SUBSYSTEM('NDR_MISC', '')
bld.SAMBA_SUBSYSTEM('NDR_SAMR', '')
bld.SAMBA_SUBSYSTEM('SCHANNELDB', '')
bld.SAMBA_SUBSYSTEM('pyldb_util', '')
bld.SAMBA_SUBSYSTEM('TORTURE_LDB_MODULE', '')


bld.RECURSE('../lib/replace')
bld.RECURSE('../lib/talloc')
bld.RECURSE('../lib/tdb')
bld.RECURSE('../lib/tevent')
bld.RECURSE('lib/ldb')
bld.RECURSE('dynconfig')
bld.RECURSE('../lib/util/charset')
bld.RECURSE('scripting/python')
bld.RECURSE('param')
bld.RECURSE('librpc')
bld.RECURSE('dsdb')
bld.RECURSE('smbd')
bld.RECURSE('cluster')
bld.RECURSE('smbd')
bld.RECURSE('libnet')
bld.RECURSE('auth')
bld.RECURSE('../lib/iniparser/src')
bld.RECURSE('../nsswitch')
bld.RECURSE('../nsswitch/libwbclient')
bld.RECURSE('lib/samba3')
bld.RECURSE('lib/socket')
bld.RECURSE('lib/ldb-samba')
bld.RECURSE('lib/tls')
bld.RECURSE('lib/registry')
bld.RECURSE('lib/messaging')
bld.RECURSE('lib/events')
bld.RECURSE('lib/cmdline')
bld.RECURSE('../lib/socket_wrapper')
bld.RECURSE('../lib/nss_wrapper')
bld.RECURSE('../lib/uid_wrapper')
bld.RECURSE('../lib/popt')
bld.RECURSE('lib/stream')
bld.RECURSE('../lib/util')
bld.RECURSE('../lib/tdr')
bld.RECURSE('../lib/tsocket')
bld.RECURSE('../lib/crypto')
bld.RECURSE('../lib/torture')
bld.RECURSE('../lib/zlib')
bld.RECURSE('lib')
bld.RECURSE('lib/com')
bld.RECURSE('smb_server')
bld.RECURSE('rpc_server')
bld.RECURSE('ldap_server')
bld.RECURSE('web_server')
bld.RECURSE('winbind')
bld.RECURSE('nbt_server')
bld.RECURSE('wrepl_server')
bld.RECURSE('cldap_server')
bld.RECURSE('ntp_signd')
bld.RECURSE('utils/net')
bld.RECURSE('utils')
bld.RECURSE('ntvfs')
bld.RECURSE('ntptr')
bld.RECURSE('torture')
bld.RECURSE('../librpc')
bld.RECURSE('client')
bld.RECURSE('libcli')
bld.RECURSE('../libcli/smb')
bld.RECURSE('../libcli/cldap')
bld.RECURSE('kdc')
bld.RECURSE('../lib/smbconf')
bld.RECURSE('../lib/async_req')
bld.RECURSE('../libcli/security')
bld.RECURSE('../libcli/ldap')
bld.RECURSE('../libcli/nbt')
bld.RECURSE('../libcli/auth')
bld.RECURSE('../libcli/drsuapi')
bld.RECURSE('../libcli/samsync')
bld.RECURSE('lib/policy')
bld.RECURSE('../libcli/named_pipe_auth')
bld.RECURSE('heimdal_build')
bld.RECURSE('lib/smbreadline')
bld.RECURSE('../codepages')
bld.RECURSE('setup')
bld.RECURSE('scripting')
bld.RECURSE('../pidl')
bld.RECURSE('../lib')

# install some extra empty directories
bld.INSTALL_DIRS("", "${LOCKDIR} ${SYSCONFDIR} ${LOCKDIR} ${PIDDIR} ${LOCALSTATEDIR}/lib ${PRIVATEDIR}/smbd.tmp/messaging")
