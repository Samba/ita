#! /usr/bin/env python

import os, shutil, re
import Task, Utils, Options, Build

"""
Execute a method to reduce the cache size after the build is complete
Looking at the cache size is an expensive operation

Do export WAFCACHE=/tmp/foo-xyz where xyz represents the cache size in megabytes
If missing, the default cache size will be set to 2GB
"""

re_num = re.compile('[a-zA-Z_]+(\d+)')

def dsize(root):
	tot = 0
	for (p, d, files) in os.walk(root):
		for f in files:
			try:
				tot += os.path.getsize(os.path.join(p, f))
			except:
				pass
	return tot / (1024*1024.0)

def cmp2(x, y):
	return cmp(x[1], y[1])

old = Build.BuildContext.compile
def compile(self):

	try:
		old(self)
	finally:

		if Options.cache_global and not Options.options.nocache:
			CACHESIZE = 2000 # 2GB +-300Mb should be enough
			val = re_num.sub('\\1', os.path.basename(Options.cache_global))
			try:
				CACHESIZE = int(val)
			except:
				pass

			for x in range(5):

				# loop in case another process is still filling up the cache
				s = dsize(Options.cache_global)
				if s < CACHESIZE:
					break

				lst = [Options.cache_global + os.sep + x for x in Utils.listdir(Options.cache_global)]

				acc = []
				for x in lst:
					try:
						acc.append((x, os.path.getctime(x), dsize(x)))
					except:
						pass

				# sort the files by the oldest first
				acc.sort(cmp=cmp2)

				tot = sum([x[2] for x in acc])
				cur = 0

				# remove at least 10% more, just to make sure
				while tot - cur > 0.9 * CACHESIZE:
					x = acc.pop(0)
					cur += x[2]

					# ignore if the folders cannot be removed
					try:
						shutil.rmtree(x[0])
					except:
						pass

Build.BuildContext.compile = compile

